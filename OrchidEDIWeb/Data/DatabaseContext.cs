﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace OrchidEDIWeb.Data
{
    public class DatabaseContext:DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options):base(options)
        {

        }

        public DbSet<TblUser> TblUser { get; set; }
        public DbSet<tblFile> tblFile { get; set; }
        public DbSet<lkpFileStatus> lkpFileStatus { get; set; }
        public DbSet<tblClient> tblClient { get; set; }
        public DbSet<tbxUserClient> tbxUserClient { get; set;}
    }
}
