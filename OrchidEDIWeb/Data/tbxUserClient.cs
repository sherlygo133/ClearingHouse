﻿using System.ComponentModel.DataAnnotations;

namespace OrchidEDIWeb.Data
{
    public class tbxUserClient
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public int ClientId { get; set; }
        [Required]
        public bool IsActive { get; set; }
    }
}
