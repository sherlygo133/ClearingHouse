﻿using System.ComponentModel.DataAnnotations;

namespace OrchidEDIWeb.Data
{
    public class lkpFileStatus
    {
        [Key]
        [Required]
        public long FileStatusID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
