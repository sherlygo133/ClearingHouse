﻿using System.ComponentModel.DataAnnotations;

namespace OrchidEDIWeb.Data
{
    public class tblClient
    {
        [Key]
        [Required]
        public int ClientID { get; set; }
        [Required(ErrorMessage = "*"), MaxLength(50)]
        public string? Name { get; set; }
        [Required(ErrorMessage = "*"), MaxLength(10)]
        public string? FederalID { get; set; }
        [MaxLength(55)]
        public string? Address1 { get; set; }
        [MaxLength(55)]
        public string? Address2 { get; set; }
        [MaxLength(50)]
        public string? City { get; set; }
        [MaxLength(2)]
        public string? State { get; set; }
        [MaxLength(5)]
        public string? Zip { get; set; }
        [MaxLength(4)]
        public string? ZipPlus4 { get; set; }
        [MaxLength(10)]
        public string? Phone { get; set;}
        [MaxLength(30)]
        public string? ReferenceID1 { get; set; }
        [MaxLength(30)]
        public string? ReferenceID2 { get; set;}
        [MaxLength(30)]
        public string? ReferenceID3 { get; set;}
        [MaxLength(30)]
        public string? ReferenceID4 { get; set;}
        [MaxLength(30)]
        public string? ReferenceID5 { get; set;}
        public DateTime RegisterDate { get; set; }
        public int ChangedBy { get;set; }
        public DateTime ChangedDate { get; set; }
        public bool IsActive { get; set; }

    }
}
