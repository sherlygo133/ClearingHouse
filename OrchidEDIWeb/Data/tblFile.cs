﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrchidEDIWeb.Data
{
    public class tblFile
    {
        [Key]
        [Required]
        public long FileID { get; set; }
        public long ClientID { get; set; }
        [Required, MaxLength(200)]
        public string? FileName { get; set; }
        [Required]
        public DateTime ReceivedDate { get; set; }
        [Required]
        public int FileStatusID { get; set; }
        //[NotMapped]
        public string? FileStatusName { get; set; }

    }
}
