﻿namespace OrchidEDIWeb.Data.Model
{
    public class ClaimList
    {
        public string FileName { get; set; }
        public DateTime SubmitDate { get; set; }
        public DateTime DOS { get; set; }
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public int ProviderId { get; set; }
        public string ProviderName { get; set; }
        public int InsuranceId { get; set; }
        public string PayerName { get; set; }
        public string ClaimStatus { get; set; }

    }
}
