﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace OrchidEDIWeb.Data
{
    public class TblUser
    {
        [Required]
        public int Id { get; set; }
        [Required(ErrorMessage = "*"), MaxLength(100)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "*"), MaxLength(100)] 
        public string LastName { get; set; }
        [Required(ErrorMessage = "*"), MaxLength(100)]
        //[DisplayName, "User Name"]
        public string UserName { get; set; }
        [Required(ErrorMessage = "*"), MaxLength(100)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "*"), EmailAddress, MaxLength(100)] 
        public string? Email { get; set; }    
                

    }
}
