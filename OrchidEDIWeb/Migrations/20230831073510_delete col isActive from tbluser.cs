﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace OrchidEDIWeb.Migrations
{
    /// <inheritdoc />
    public partial class deletecolisActivefromtbluser : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "TblUser");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "TblUser",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
