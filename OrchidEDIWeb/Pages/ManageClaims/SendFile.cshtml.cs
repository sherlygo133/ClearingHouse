using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;
using OrchidEDIWeb.Data;
using Serilog;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace OrchidEDIWeb.Pages.ManageClaims
{
    [Authorize]
    public class SendFileModel : PageModel
    {
        private IHostingEnvironment _environment;
        private readonly DatabaseContext _ctx;
        private readonly IConfiguration _config;

        public SendFileModel(IHostingEnvironment environment, DatabaseContext ctx, IConfiguration config)
        {
            _environment = environment;
            _ctx = ctx;
            _config = config;
        }
        [BindProperty]
        public IFormFile Upload { get; set; }
        public IEnumerable<Data.tblFile> Files { get; set; }
        public IEnumerable<string> Pages { get; set; }
        public async Task OnPostUploadFileAsync()
        {
            try
            {
                string fileName = Path.GetFileName(Upload.FileName);
                //Cannot have duplicate file name in the same day
                var vExistingFile = _ctx.tblFile.Where(t => t.FileName == fileName && 
                        t.ReceivedDate >= DateTime.Now.Date && t.ReceivedDate< DateTime.Now.AddDays(1).Date)
                        .Select(t => new {
                            t.FileID
                        }).ToList();
                if (vExistingFile.Count > 0)
                    TempData["error"] = "File already uploaded.";
                else
                {
                    //Path for keep Upload Files: UploadFiles\[clientID]\[uploadDate(yyyyddMM)]\
                    string todayDate = DateTime.Now.ToString("yyyyddMM");
                    int clientID = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("ClientID").Value);
                    string wwwPath = _environment.WebRootPath;
                    string contentPath = _environment.ContentRootPath;
                    string path = Path.Combine(wwwPath, "UploadFiles");
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    path = Path.Combine(path, todayDate);
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);
                    path = Path.Combine(path, clientID.ToString());
                    if (!Directory.Exists(path))
                        Directory.CreateDirectory(path);

                    var file = Path.Combine(path, Upload.FileName);
                    using (var fileStream = new FileStream(file, FileMode.Create))
                    {
                        await Upload.CopyToAsync(fileStream); //This one will replace the existing file.
                    }

                    _ctx.Database.ExecuteSqlRaw("Insert into tblFile ([ClientID],[FileName],[ReceivedDate],[FileStatusID]) Values({0},{1},{2},{3})", clientID,fileName, DateTime.Now, 1);
                    
                    TempData["success"] = fileName + " was received successfully.";
                }
                HttpContext.Session.SetInt32("PageNumber", 1);
                GetFiles();
            }
            catch(Exception ex)
            {
                TempData["error"] = "Error has accured. Please contact administrator.";
                Log.Error("Login Error: " + ex.Message);
            }
        }
        public Task OnGet()
        {
            HttpContext.Session.SetInt32("PageNumber", 1);
            GetFiles();
            return Task.CompletedTask;
        }

        public void OnPostPageNavigate(string nav)
        {
            int totalPages = (int)HttpContext.Session.GetInt32("TotalPages");
            int currentPageNumber = (int)HttpContext.Session.GetInt32("PageNumber");
            int nextPageNumber = 1;
            switch (nav)
            {
                case "first":
                    HttpContext.Session.SetInt32("PageNumber", 1);
                    break;
                case "prev":
                    nextPageNumber = (currentPageNumber > 1) ? currentPageNumber - 1 : 1;
                    HttpContext.Session.SetInt32("PageNumber", nextPageNumber);
                    break;
                case "next":
                    nextPageNumber = (currentPageNumber < totalPages) ? currentPageNumber + 1 : totalPages;
                    HttpContext.Session.SetInt32("PageNumber", nextPageNumber);
                    break;
                case "last":
                    HttpContext.Session.SetInt32("PageNumber", totalPages);
                    break;
            }
            GetFiles();
            //TempData["error"] = $"{nav}";
        }
        private void GetFiles()
        {
            ViewData["PageNo"] = HttpContext.Session.GetInt32("PageNumber").ToString();
            SqlParameter[] sqlParameters = new SqlParameter[7];
            sqlParameters[0] = new SqlParameter("@searchBy", System.Data.SqlDbType.VarChar);
            sqlParameters[0].Value = "FileName";
            sqlParameters[1] = new SqlParameter("@keyword", System.Data.SqlDbType.VarChar);
            sqlParameters[1].Value = "";
            sqlParameters[2] = new SqlParameter("@sortBy", System.Data.SqlDbType.VarChar);
            sqlParameters[2].Value = "TF.ReceivedDate";
            sqlParameters[3] = new SqlParameter("@viewOrder", System.Data.SqlDbType.Bit);
            sqlParameters[3].Value = 1;
            sqlParameters[4] = new SqlParameter("@pageSize", System.Data.SqlDbType.Int);
            sqlParameters[4].Value = _config.GetValue<int>("PageSize");
            sqlParameters[5] = new SqlParameter("@pageNumber", System.Data.SqlDbType.Int);
            sqlParameters[5].Value = HttpContext.Session.GetInt32("PageNumber");
            sqlParameters[6] = new SqlParameter("@totalPage", System.Data.SqlDbType.Int);
            sqlParameters[6].Direction = System.Data.ParameterDirection.Output;

            Files = _ctx.tblFile.FromSqlRaw($"EXEC [dbo].[tblFileSelectAllFilter] @searchBy, @keyword, @sortBy, @viewOrder, @pageSize, @pageNumber, @totalPage OUTPUT",
                sqlParameters).ToList();

            int totalPage = (int)sqlParameters[6].Value;
            HttpContext.Session.SetInt32("TotalPages", totalPage);
            //Pages = new IEnumerable<string>();
            List<string> strPages = new List<string>();
            for (int i = 1; i <= totalPage; i++)
                strPages.Add(i.ToString());

            Pages = strPages.ToList();
        }
    }
}
