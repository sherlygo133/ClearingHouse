using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace OrchidEDIWeb.Pages.ManageClaims
{
    [Authorize]
    public class ClaimHistoryModel : PageModel
    {
        public void OnGet()
        {
        }
        public void OnPostSearch()
        {
            string searchBy = Request.Form["ddlSearchBy"];            
            string filterName = Request.Form["txtFilterName"];

            string filterToggleAccountNo = Request.Form["btnFilterAcctNoToggle"];
            string filterAcctNoStart = Request.Form["txtFilterAcctNoFrom"];
            string filterAcctNoTo = Request.Form["txtFilterAcctNoTo"];

            string filterToggleDOS = Request.Form["btnFilterDOSToggle"];
            string filterDOSStart = Request.Form["dateDosStart"];
            string filterDOSEnd = Request.Form["dateDosStart"];

            string filterToggleSubmitDate = Request.Form["btnFilterSubmitDateToggle"];
            string filterSubmitStart = Request.Form["dateSubmitStart"];
            string filterSubmitEnd = Request.Form["dateSubmitEnd"];
            
            //Search
        }
    }
}
