using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OrchidEDIWeb.Data;
using System.Security.Claims;

namespace OrchidEDIWeb.Pages.User
{
    [Authorize]
    //[Authorize(Policy = "Admin")]  // <-- "Admin" (Role)
    public class CreateModel : PageModel
    {
        private readonly DatabaseContext _ctx;

        public CreateModel(DatabaseContext ctx)
        {
            _ctx = ctx;
        }
        [BindProperty]
        public Data.TblUser TblUser { get; set; }
        [BindProperty]
        public Data.tbxUserClient UserClient { get; set; }
        public IActionResult OnGet()
        {
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if(!ModelState.IsValid)
                return Page();

            var transaction = _ctx.Database.BeginTransaction();
            try
            {
                if(TblUser == null || UserClient == null)
                    return NotFound();
                _ctx.TblUser.Add(TblUser);
                _ctx.SaveChanges();

                UserClient.ClientId = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("ClientID").Value);
                UserClient.UserId = TblUser.Id;
                _ctx.tbxUserClient.Add(UserClient);
                _ctx.SaveChanges();

                transaction.Commit();

                TempData["success"] = "Saved successfully";
                return RedirectToPage("DisplayAll");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Error has accured";
                return Page();
            }
            //return RedirectToPage(); //This is for back to page with emtpy fields
        }
    }
}
