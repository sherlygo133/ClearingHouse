using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OrchidEDIWeb.Data;
using Serilog;
using System.Security.Claims;

namespace OrchidEDIWeb.Pages.User
{
    [Authorize]
    public class EditModel : PageModel
    {
        private readonly DatabaseContext _ctx;

        public EditModel(DatabaseContext ctx)
        {
            _ctx = ctx;
        }
        [BindProperty]
        public Data.TblUser TblUser { get; set; }
        [BindProperty]
        public Data.tbxUserClient UserClient { get; set; }
        public IActionResult OnGet(int id)
        {
            var user = (from x in _ctx.tbxUserClient 
                            join u in _ctx.TblUser on x.UserId equals u.Id 
                            join c in _ctx.tblClient on x.ClientId equals c.ClientID 
                            where x.UserId == id
                       select new { UserClientId = x.Id, ClientId = x.ClientId, Id = u.Id, LastName = u.LastName, FirstName = u.FirstName, UserName = u.UserName, Password = u.Password, Email = u.Email, IsActive = x.IsActive }).First();
            //var user = _ctx.TblUser.Find(id);
            if(user == null)
                return NotFound();
            TblUser = new TblUser();
            TblUser.Id = user.Id;
            TblUser.LastName = user.LastName;
            TblUser.FirstName = user.FirstName; 
            TblUser.UserName = user.UserName;
            TblUser.Password = user.Password;
            TblUser.Email = user.Email;

            UserClient = new tbxUserClient();
            UserClient.Id = user.UserClientId;
            UserClient.UserId = user.Id;
            UserClient.ClientId = user.ClientId;
            UserClient.IsActive = user.IsActive;
            
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
                return Page();

            var transaction = _ctx.Database.BeginTransaction();
            try
            {
                if (TblUser == null || UserClient == null)
                    return NotFound();

                _ctx.TblUser.Update(TblUser);
                _ctx.SaveChanges();

                _ctx.tbxUserClient.Update(UserClient);
                _ctx.SaveChanges();

                transaction.Commit();
                TempData["success"] = "Edit successfully";

                ////First get user claims, to get login information
                //var claims = ClaimsPrincipal.Current.Identities.First().Claims.ToList();

                ////Filter specific claim    
                //claims?.FirstOrDefault(x => x.Type.Equals("UserName", StringComparison.OrdinalIgnoreCase))?.Value
                //    .ToString();

                return RedirectToPage("DisplayAll");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Error has accured. Please contact Administrator.";
                Log.Error("Edit User Error: " + ex.Message);
                return Page();
            }
        }
    }
}
