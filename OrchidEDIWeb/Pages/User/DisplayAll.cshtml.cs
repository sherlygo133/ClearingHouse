using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Data.SqlClient;
using OrchidEDIWeb.Data;
using System.Data;
using System.Security.Claims;

namespace OrchidEDIWeb.Pages.User
{
    [Authorize]
    //[Authorize(Policy = "UserPolicy")]
    //[Authorize(Roles = "Admin")]
    public class DisplayAllModel : PageModel
    {
        private readonly DatabaseContext _ctx;

        public DisplayAllModel(DatabaseContext ctx)
        {
            _ctx = ctx;
        }
        public List<Data.TblUserEx> Users { get; set; }
        
        public IActionResult OnGet()
        {
            ClaimsPrincipal principal = HttpContext.User as ClaimsPrincipal;
            if (null != principal)
            {
                foreach (Claim claim in principal.Claims)
                {
                    string str = ("CLAIM TYPE: " + claim.Type + "; CLAIM VALUE: " + claim.Value + "</br>");
                }
            }

            LoadUserList();
            //Users = _ctx.TblUser.Where(u => u.ClientID == clientID).ToList();
            return Page();
        }

        public Task<IActionResult> OnPostDeleteAsync(int userClientId, int id)
        {
            var user = _ctx.TblUser.Find(id);
            var userClient = _ctx.tbxUserClient.Find(userClientId);
            if(user == null || userClient == null)
                return Task.FromResult<IActionResult>(NotFound());

            var transaction = _ctx.Database.BeginTransaction();
            try
            {
                //Dont delete user data, because its related with other tables (createdBy, changedBy etc)
                //_ctx.TblUser.Remove(user);
                //_ctx.SaveChanges();
                
                _ctx.tbxUserClient.Remove(userClient);
                _ctx.SaveChanges();

                transaction.Commit();

                TempData["success"] = "Deleted Successfully";
            }
            catch (Exception ex)
            {
                TempData["error"] = "Error on deleteing record";                    
            }
            return Task.FromResult<IActionResult>(RedirectToPage());
        }

        private void LoadUserList()
        {
            SqlParameter[] sqlParams = new SqlParameter[1];
            sqlParams[0] = new SqlParameter("@ClientId", SqlDbType.VarChar);
            sqlParams[0].Value = int.Parse(((ClaimsIdentity)User.Identity).FindFirst("ClientID").Value);
            DataTable dtUsers = DbContextExtensions.ExecuteQuery(_ctx, "tblUserSelectAllByClient", sqlParams, "StoreProcedure");
            Users = new List<TblUserEx>();
            for(int i= 0; i < dtUsers.Rows.Count; i++)
            {
                TblUserEx user = new TblUserEx();
                user.Id = Convert.ToInt32(dtUsers.Rows[i]["id"]);
                user.LastName = dtUsers.Rows[i]["LastName"].ToString()!;
                user.FirstName = dtUsers.Rows[i]["FirstName"].ToString()!;
                user.UserName = dtUsers.Rows[i]["UserName"].ToString()!;
                user.Email = dtUsers.Rows[i]["Email"].ToString()!;
                user.IsActive = Convert.ToBoolean(dtUsers.Rows[i]["IsActive"]);
                user.UserClientId = Convert.ToInt32(dtUsers.Rows[i]["UserClientId"]);

                this.Users.Add(user);
            }
        }
    }
}
