using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using OrchidEDIWeb.Data;

namespace OrchidEDIWeb.Pages.Account
{
    public class LogoutModel : PageModel
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly DatabaseContext _ctx;
        public LogoutModel(DatabaseContext ctx, IHttpContextAccessor httpContextAccessor)
        {
            _ctx = ctx;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<IActionResult> OnGetAsync()
        {
            await _httpContextAccessor.HttpContext.SignOutAsync("OAuthScheme");

            return Redirect("/Login");

        }
    }
}
