using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.IdentityModel.Tokens;
using OrchidEDIWeb.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using Serilog;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Data.Common;
using Microsoft.Data.SqlClient;

namespace OrchidEDIWeb.Pages.Account
{
    public class LoginModel : PageModel
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly DatabaseContext _ctx;
        IConfiguration _configuration;

        public LoginModel(DatabaseContext ctx, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _ctx = ctx;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }

        [BindProperty]
        public Data.TblUserEx Account { get; set; }        
        public List<SelectListItem> ClientOptions { get; set; }
        public IActionResult OnGet()
        {
            InitializeForm();
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            try
            {
                string clientID = Request.Form["ddlClients"].ToString();
                SqlParameter[] sqlParams = new SqlParameter[3];
                sqlParams[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
                sqlParams[0].Value = Account.UserName;
                sqlParams[1] = new SqlParameter("@Password", SqlDbType.VarChar);
                sqlParams[1].Value = Account.Password;
                sqlParams[2] = new SqlParameter("@ClientId", SqlDbType.Int);
                sqlParams[2].Value = clientID;

                DataTable dtUserLogin = DbContextExtensions.ExecuteQuery(_ctx, "tblUserAuthenticate", sqlParams, "StoreProcedure");

                //TblUser user = _ctx.TblUser.Where(u => u.UserName == Account.UserName && u.Password == Account.Password && u.ClientID == int.Parse(clientID) && u.IsActive == true).FirstOrDefault()!;
                if (dtUserLogin == null || dtUserLogin.Rows.Count == 0)
                {
                    TempData["error"] = "Username or Password not correct.";
                    InitializeForm();
                    return Page();
                }
                //TempData["success"] = "Login successfully";
                //TempData["success"] = GetToken(user);
                var claims = new List<Claim>
                    {
                    //Later, the credential value must be encrypted. Ex: userId, username, email
                        new Claim(ClaimTypes.NameIdentifier, dtUserLogin.Rows[0]["Id"].ToString()!),
                        new Claim(ClaimTypes.Name, dtUserLogin.Rows[0]["UserName"].ToString()!),
                        new Claim(ClaimTypes.Email, dtUserLogin.Rows[0]["Email"].ToString()!),
                        new Claim("FullName", dtUserLogin.Rows[0]["LastName"].ToString() + ", " + dtUserLogin.Rows[0]["FirstName"].ToString()),
                        new Claim("ClientID", clientID)
                        //new Claim("User", "Add")   //  <--- "User" (Module) "Add" (Action); later will be pull up from DB Table UserRole
                        // add or remove claims as necessary    
                    };
                var claimsIdentity = new ClaimsIdentity(claims, "OAuthScheme");
                //var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);
                //// Set current principal
                //Thread.CurrentPrincipal = claimsPrincipal;

                await _httpContextAccessor.HttpContext
                    .SignInAsync("OAuthScheme",
                        new ClaimsPrincipal(claimsIdentity),
                        new AuthenticationProperties());
                Log.Information("ALOHA {@user}", dtUserLogin.Rows[0]["UserName"].ToString());
                return Redirect("/Index");

            }
            catch (Exception ex)
            {
                TempData["error"] = "Error has accured";
                Log.Error("Login Error: "+ex.Message);
                return Page();
            }
        }

        private void InitializeForm()
        {
            ClientOptions = _ctx.tblClient.Select(a =>
                                  new SelectListItem
                                  {
                                      Value = a.ClientID.ToString(),
                                      Text = a.Name
                                  }).ToList();
        }
        // public virtual System.Security.Claims.ClaimsPrincipal User { get; }
        //This one for JWT
        //public string GetToken(Data.TblUser account)
        //{
        //    var claims = new[] {
        //                new Claim(JwtRegisteredClaimNames.Sub, _configuration["JwtSettings:Subject"]!),
        //                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
        //                new Claim(JwtRegisteredClaimNames.Iat, DateTime.UtcNow.ToString()),
        //                new Claim("UserId", account.Id.ToString()),
        //                new Claim("UserName", account.UserName!),
        //                new Claim("Email", account.Email!)
        //            };
        //    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtSettings:Key"]!));
        //    var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
        //    var token = new JwtSecurityToken(
        //        _configuration["JwtSettings:Issuer"],
        //        _configuration["JwtSettings:Audience"],
        //        claims,
        //        expires: DateTime.UtcNow.AddMinutes(10),
        //        signingCredentials:signIn);

        //    string strToken = new JwtSecurityTokenHandler().WriteToken(token);
        //    return strToken;
        //}
    }
}
