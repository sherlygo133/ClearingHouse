using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using OrchidEDIWeb.Data;
using Serilog;
using System.Security.Claims;

namespace OrchidEDIWeb.Pages.Account
{
    public class RegisterModel : PageModel
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly DatabaseContext _ctx;
        IConfiguration _configuration;
        public RegisterModel(DatabaseContext ctx, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _ctx = ctx;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }
        [BindProperty]
        public Data.tblClient Client { get; set; }
        [BindProperty]
        public Data.TblUserEx User { get; set; }
        [BindProperty]
        public Data.tbxUserClient UserClient { get; set; }

        public IActionResult OnGet()
        {
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            var transaction = _ctx.Database.BeginTransaction();
            try
            {
                if (Client == null || User == null)
                    return NotFound();
                
                Client.RegisterDate = Client.ChangedDate = DateTime.Now;
                _ctx.tblClient.Add(Client);
                _ctx.SaveChanges();

                _ctx.TblUser.Add(User);
                _ctx.SaveChanges();

                UserClient = new tbxUserClient();
                UserClient.ClientId = Client.ClientID;
                UserClient.UserId = User.Id;
                UserClient.IsActive = false;
                _ctx.tbxUserClient.Add(UserClient);
                _ctx.SaveChanges();

                transaction.Commit();

                TempData["success"] = "Register successfully. We are preparing the system.";
                return RedirectToPage("Login");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Error has accured";
                Log.Error("Register Error: " + ex.StackTrace);
                return Page();
            }
        }
    }
}
